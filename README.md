# Guías desarrolladores Red5G

En este repositorio encontraremos guías y recursos para todos los desarrolladores, así como el proceso de iniciación para los nuevos integrantes del equipo. Está es una segunda versión de los estandares de codificación. La primera versión la pueden encontrar [aquí](https://docs.google.com/presentation/d/12esVf9uU4SK-Egk8Xyh73glhf6vzbm2A67e_huy4o5w/edit?usp=sharing){:target="_blank"}.

----

## Estandares de codificación

Manejaremos estandares de codificación para cada uno de los lenguajes utilizados en cada uno de los proyectos, debes tener en cuenta que su adopción es **obligatorio** por lo tanto es necesario dirigirse a las guías si aún no estas familiarizado con el estandar del lenguaje de turno.

El listado de los estandares con sus respectivos lenguajes son:

* [Estandar PHP](standar/php.md)
* [Estandar JS](standar/js.md)
* [Estandar HTML](standar/html.md)
* [Estandar CSS](standar/css.md)
* [Estandar TypeScript](standar/ts.md)
* [Estandar Python](standar/python.md)

----

## Iniciación nuevos desarrolladores

Si eres un nuevo integrante del equipo de desarrollo, está sección será una gran ayuda casi obligatoria para que la integración al grupo y para ayudarte a que puedas adaptarte más facilmente a la hora de trabajar en un desarrollo.

El paso a paso que debes realizar es el siguiente:

* [Instalación de herramientas](tools/tools.md)
* [Git](tools/git.md)
* [Practicas Git](practice/git.md)
* [Servicios](guide/services.md)
* Practicas servicios

----

## Herramientas

En esta sección colocamos una guía para la instalación de las herramientas necesarias para poder realizar nuestro trabajo, pronto incluiremos el versionamiento de las herramientas por si necesitas una versión en especifica. El listado de herramientas son:

* [VirtualBox](tools/tools.md)
* [S.O. CentOS 7/8](tools/tools.md)
* Doker
* Editores recomendados
* Gui Mysql

----

## Guía de buenas practicas **obligatorias :D**

En esta guía damos unos lineamientos a tener en cuenta cuando estes trabajando en algún proyecto, se trata de evitar posibles problemas a futuro y recomendaciones (obligatorias) que nos servirán para tener un mejor resultado en nuestro trabajo. [click](gpractice.md).

----

## Guía de desarrollos de funcionalidades

Uno de los principios de programación es DRY (Don't Repeat You), lo aplicamos para no repetir código innecesariamente en los proyectos y lo llevamos a un siguiente nivel. Si otro compañero del equipo ya ha desarrollado previamente una funcionalidad y tú la necesitas, puedes reutilizarla usando esta guía, en donde encontraras el repositorio donde está alojada, su documentación y las recomendaciones a tener en cuenta, es de entender que está sección irá en crecimiento constante. [click](functions.md)

----
