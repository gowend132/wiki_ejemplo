# Practicas Git

Ya habiendo instalado y configurado git, pasaremos a la practica, pero antes debemos aprender cuales son los comandos más útiles para poder realizar las practicas, a continuación te dejaremos un listado de recursos para que conozcas y PRACTIQUES los comandos básicos de git.

* https://rogerdudler.github.io/git-guide/index.es.html
* https://programmerclick.com/article/7862924408/
* https://www.it-swarm-es.com/es/git/git-para-principiantes-la-guia-practica-definitiva./958068583/
* https://www.hostinger.co/tutoriales/comandos-de-git
* https://www.youtube.com/watch?v=kEPF-MWGq1w
* https://www.youtube.com/watch?v=3XlZWpLwvvo
* https://www.youtube.com/watch?v=gyXldK4vc40

Una vez completado este punto, debes notificar a tu lider de equipo para continuar con la siguiente parte de las practicas en donde ya incluimos un proyecto.

## Practica #1

Con el conocimiento adquirido, vamos a crear un reporsitorio (con tu cuenta de Bitbucket) llamado PRACTICA 1 y desarrollaremos el siguiente proyecto con cada uno de sus puntos en el lenguaje de nuestra preferencia:

1. Se debe crear un formulario donde se puedan crear los articulos (comida) que seran vendidos, debe contener, el nombre y el valor.

2. Se debe crear un formulario de pedidos de un restaurante, los campos que debe tener son los siguientes: Nombre del cliente, fecha y hora del pedido, articulo (debe cargar los articulos registrados en el formulario anterior), cantidad, valor total, dirección, ciudad, telefono del cliente.

3. Se debe tener un listado de todos los pedidos recibidos y por estado. Ej: Acabamos de recibir un pedido y se debe registrar con el estado EN ESPERA, una vez que se empiece a preparar, se debe cambiar el estado a EN PREPARACIÓN, cuando este se haya finalizado y pase a entrega de domiciliario se debe cambiar el estado a EN REPARTO y cuando sea entregado al cliente, se debe cambiar el estado a ENTREGADO. Se debe llevar registro de fecha y hora de cada uno de los cambios de estado.

En la medida que vayamos realizando los puntos, iremos subiendo el código al repositorio con los comandos que hemos aprendido. El tiempo para completar esta practica es de 3 hs. Una vez terminado todo, informa a tu lider de equipo la finalización de esta practica.

[Volver al inicio](../README.md)
