# Estandar Python

El estandar para Python que vamos a implementar en nuestros proyectos es el [PEP 8](https://www.python.org/dev/peps/pep-0008/){:target="_blank"}, este estandar es aceptado a nivel general por toda la comunidad de Python y es el que usaremos en el desarrollo de las lambdas de AWS.

En el encontraremos una completa guía de estilo para todo el lenguaje de Python.

Otros puntos para agregar en el estandar de Python son los siguientes:

- En el uso de las lambdas, la logica y manejo de datos debe ser llevado en clases y metodos, no en el archivo handler.
- Se debe comentar todas las clases y metodos explicando brevemente su funcionamiento.

[Volver al inicio](../README.md)
