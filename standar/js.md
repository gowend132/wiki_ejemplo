# Estandar JS

El estandar que vamos a implementar para JavaScript es el que nos provee [Standar JS](https://standardjs.com/){:target="_blank"}, es usado por muchos proyectos, puedes ver la lista en el siguiente enlace [click](https://standardjs.com/#who-uses-javascript-standard-style){:target="_blank"} y debido a su uso en tan importantes desarrollos, lo adoptaremos para nuestros proyectos.

Una ventaja que nos presenta para quienes usen [Visual Code](https://code.visualstudio.com/){:target="_blank"} es que nos permite instalarlo como una [extensión](https://marketplace.visualstudio.com/items?itemName=chenxsan.vscode-standardjs){:target="_blank"} y nos ayudara ~~obligará~~ a escribir bien el código.

Las reglas que nos indica el estandar son [StandarJS Rules.](https://standardjs.com/rules.html){:target="_blank"}

[Volver al inicio](../README.md)
