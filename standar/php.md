# Estandar PHP

El estandar para PHP que vamos a implementar en nuestros proyectos es el que nos provee [PHP-FIG](https://www.php-fig.org/){:target="_blank"} llamado PSR, este estandar es aceptado a nivel general por toda la comunidad de PHP y es el que se implementa en muchos Framewroks tales como Laravel.

---- 


### Basic conding standar PSR-1

El estandar PSR-1 nos indica como se debe implementar la codificación. [PSR-1](https://www.php-fig.org/psr/psr-1/){:target="_blank"}


----


### Extend conding style PSR-12

El estandar PSR-12 nos muestra la guía de estilo que debemos tener en cuanta al momento de codificar, cabe aclarar que es una extención del estandar PSR-1. [PSR-12](https://www.php-fig.org/psr/psr-12/){:target="_blank"}


----


### Otros estandares PSR

Si bien es cierto los estandares más importantes (PSR-1 y PSR-12) deben ser implementados de forma obligatoria en nuestro codigo, existen otros estandares que nos guían en la forma de llevar a cabo algunas funcionalidades, podrás observarlos [aquí](https://www.php-fig.org/psr/){:target="_blank"}.


[Volver al inicio](../README.md)
