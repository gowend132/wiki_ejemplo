# Estandar HTML

El estandar para HTML que vamos a implementar en nuestras vistas es el estandar de [Google](https://google.github.io/styleguide/htmlcssguide.html){:target="_blank"}. Si bien es cierto HTML tiene una curva de aprendizaje muy rapida y podemos catalogarlo de "sencillo", la idea es codificarlo de la forma correcta.

Las reglas de HTML son [Google HTML/CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.html#HTML){:target="_blank"}.

[Volver al inicio](../README.md)
