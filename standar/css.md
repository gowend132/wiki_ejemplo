# Estandar CSS

El estandar para CSS que vamos a implementar en nuestros estilos y al igual que HTML es el estandar de [Google](https://google.github.io/styleguide/htmlcssguide.html){:target="_blank"}. Hagamos que nuestras hojas de estilo sigan el estandar propuesto por Google.

Las reglas de CSS son [Google HTML/CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.html#CSS){:target="_blank"}.

[Volver al inicio](../README.md)
