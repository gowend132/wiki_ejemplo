# Guía de servicios

Los siguientes enlaces servirán de guía para la iniciación en el uso de los servicios. Inicialmente aprenderemos todo lo que tiene que ver con los aspectos teoricos y practicos acerca sobre que es un API REST, luego aprenderemos a realizar autenticaciones con Auth 2.0, siguiente a eso pasamos a la arquitectura de microservicios y por ultimo veremos cosas relacionadas a AWS tales como Lambdas usando Serverless Framework.

----

## API Rest

* https://www.redhat.com/en/topics/api/what-is-a-rest-api
* https://blog.postman.com/rest-api-examples/

----

## Athenticate Auth 2.0

* https://oauth.net/getting-started/

----

## Microservices

* https://microservices.io/patterns/microservices.html
* https://aws.amazon.com/microservices/?nc1=h_ls
* https://cloud.google.com/learn/what-is-microservices-architecture#section-1
* https://blog.dreamfactory.com/microservices-examples/

----

## Serverless

* https://martinfowler.com/articles/serverless.html
* https://www.serverless.com/framework/docs/getting-started
* https://www.serverless.com/examples
* https://www.serverless.com/category/guides-and-tutorials

----

## Lambdas

* https://aws.amazon.com/lambda/
* https://docs.aws.amazon.com/lambda/latest/dg/lambda-samples.html


[Volver al inicio](../README.md)
