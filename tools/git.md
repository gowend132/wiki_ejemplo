# Instalación y configuración de GIT

Si has llegado a este punto es porque ya tienes todas las herramientas instaladas, bien, solo nos falta git. Instalaremos y lo configuraremos con las siguientes guías:

* [Instalación](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Instalaci%C3%B3n-de-Git){:target="_blank"}
* [Configuración](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Configurando-Git-por-primera-vez){:target="_blank"}

Es importante en este punto contar con una cuenta en [Bitbucket](https://bitbucket.org/){:target="_blank"}, para poder realizar las practicas que siguen.

[Volver al inicio](../README.md)
