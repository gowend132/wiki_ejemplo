# Instalación de herramientas

En este punto ya tendremos a disposición un pc para poder trabajar, así que iniciaremos con las descargas e instalaciones de las herramientas que usaremos día a día. 


## VirtualBox
Iniciaremos con la instalación de un programa para montar maquinas virtuales, en este caso será VirtualBox, ingresa al siguiente enlace para descargar e instalar el programa. [Enlace a VB](https://www.virtualbox.org/){:target="_blank"}.


## CentOS
Luego de haber descargado e instalado VirtualBox, seguimos con la instalación del S.O. en la maquina virtual, para este caso usaremos CentOS 8, en este [enlace](https://blogdesistemas.com/como-instalar-centos-8-paso-a-paso/){:target="_blank"} encontrarás un guía para hacer una instalación sencilla y sin contratiempos.

Luego de completar la instalación ejecutaremos los siguientes comandos:

* yum install curl zip unzip -y
* setenforce 0
* systemctl stop firewalld
* systemctl disable firewalld

Dependiendo de si vamos a usar PHP o Python, seguiremos un camino u otro.

## PHP

Tengamos en cuenta que ya hemos instalado Apache (indispensable para usar PHP), iremos a uno de los siguientes donde haremos el paso a paso para instalar la versión de PHP que necesitemos y cada una de las extensiones que vamos a necesitar.

* [PHP 5.6](https://webtatic.com/packages/php56/){:target="_blank"} (CentOS 7)
* [PHP 7.*](https://computingforgeeks.com/how-to-install-php-7-2-7-1-on-rhel-8/){:target="_blank"}


## Python

Antes de facilitar los enlaces de instalación, debo mencionar que si vas a desarrollar en Python, no es del todo obligatorio trabajar en una maquina virtual, dado que probablemente vas a desarrollar lambdas de AWS y en este punto puedes trabajar en Windows sin ningún problema, ya que en este momento la versión de Python con la que trabajamos es la 3.9 y usualmente los repositorios de las distros no vienen con esta versión, sino una desactualizada. En este [enlace](https://computingforgeeks.com/install-latest-python-on-centos-linux/){:target="_blank"} podrás seguir una guía para una correcta instalación de Python.

## Mysql

La base de datos que usamos es Mysql pueden ser en las versiones 5.7/8. Enlaces guías:

* [Mysql 5.7](https://computingforgeeks.com/install-mysql-5-7-on-centos-rhel-linux/){:target="_blank"}
* [Mysql 8](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-centos-8-es){:target="_blank"}

[Volver al inicio](../README.md)
